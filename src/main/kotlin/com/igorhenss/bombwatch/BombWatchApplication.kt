package com.igorhenss.bombwatch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BombWatchApplication

fun main(args: Array<String>) {
	runApplication<BombWatchApplication>(*args)
}
